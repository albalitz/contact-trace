use std::error::Error;
use std::path::PathBuf;

use chrono::{Duration, Local, NaiveDateTime};

use diesel::prelude::*;
use diesel::SqliteConnection;

diesel_migrations::embed_migrations!();

use crate::constants::DATE_ARG_FORMAT;
use crate::schema;
use crate::schema::traces::dsl::*;
use crate::traces::{NewTrace, Trace};
use crate::Config;

/// Creates the parent directories for the sqlite database if needed,
/// connects to the database,
/// runs migrations,
/// and returns a result of the established connection or an error if any occurs.
pub fn prepare_database_connection(database_url: &str) -> Result<SqliteConnection, Box<dyn Error>> {
    if let Some(database_dir) = PathBuf::from(database_url).parent() {
        std::fs::create_dir_all(database_dir)?;
    }
    let database_connection = SqliteConnection::establish(&database_url)?;
    embedded_migrations::run(&database_connection)?;
    Ok(database_connection)
}

/// Save a new trace to the database
pub fn add_trace(connection: &SqliteConnection, trace: &NewTrace) -> Result<(), Box<dyn Error>> {
    diesel::insert_into(schema::traces::table)
        .values(trace)
        .execute(connection)?;
    Ok(())
}

/// Query traces within the configured timeframe from the database
pub fn recent_traces(
    connection: &SqliteConnection,
    config: &Config,
) -> Result<Vec<Trace>, Box<dyn Error>> {
    let earliest = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0)
        - Duration::days(config.timeframe().num_days());
    let recent_traces = traces
        .filter(datetime.gt(earliest))
        .order_by(datetime.desc())
        .load::<Trace>(connection)?;
    Ok(recent_traces)
}

/// Delete traces older than the configured timeframe from the database
pub fn purge_traces(
    connection: &SqliteConnection,
    config: &Config,
) -> Result<usize, Box<dyn Error>> {
    let earliest = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0)
        - Duration::days(config.timeframe().num_days());
    Ok(diesel::delete(traces.filter(datetime.lt(earliest))).execute(connection)?)
}

/// Query a specific trace by date and name
pub fn trace_by_name_and_date(
    connection: &SqliteConnection,
    trace_name: &str,
    trace_date: &str,
) -> Result<Option<Trace>, Box<dyn Error>> {
    let matches_by_name: Vec<Trace> = traces
        .filter(name.eq(trace_name))
        .order_by(datetime.desc())
        .load(connection)?;
    Ok(matches_by_name
        .into_iter()
        .find(|t| t.datetime().format(DATE_ARG_FORMAT).to_string() == trace_date))
}

/// Delete a single trace from the database
pub fn delete_trace(connection: &SqliteConnection, t: Trace) -> Result<(), Box<dyn Error>> {
    // TODO: can diesel delete a trace without querying by its ID again?
    diesel::delete(traces)
        .filter(id.eq(t.id()))
        .execute(connection)?;
    Ok(())
}

#[cfg(test)]
mod test_add_trace {
    use super::*;

    #[test]
    fn should_save_traces_to_database() {
        let connection =
            prepare_database_connection("/tmp/contact-trace-test/add_trace_should_save.db")
                .unwrap();

        // cleanup before running in case any cleanup was not done before due to a failure
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });

        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        assert_eq!(
            t.len(),
            0,
            "Should not find any traces in database before saving"
        );

        let dt_1 = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let new_trace = NewTrace::new("Tester McTestface", dt_1, "", "");
        add_trace(&connection, &new_trace).unwrap();

        let dt_2 = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let new_trace = NewTrace::new("Tester Two", dt_2, "over there", "");
        add_trace(&connection, &new_trace).unwrap();

        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        assert_eq!(t.len(), 2, "Should find exactly two saved traces");
        assert_eq!(t[0].id(), 1);
        assert_eq!(t[0].name(), "Tester McTestface");
        assert_eq!(t[0].datetime(), &dt_1);
        assert_eq!(t[0].location(), "");
        assert_eq!(t[0].comment(), "");
        assert_eq!(t[1].id(), 2);
        assert_eq!(t[1].name(), "Tester Two");
        assert_eq!(t[1].datetime(), &dt_2);
        assert_eq!(t[1].location(), "over there");
        assert_eq!(t[1].comment(), "");

        // cleanup
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });
    }
}

#[cfg(test)]
mod test_recent_traces {
    use super::*;

    #[test]
    fn should_gather_traces_within_configured_timeframe() {
        let config = Config::default();
        let connection = prepare_database_connection(
            "/tmp/contact-trace-test/recent_traces_should_find_within_timeframe.db",
        )
        .unwrap();

        // cleanup before running in case any cleanup was not done before due to a failure
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });

        let dt_1 = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let expected_trace = NewTrace::new("Tester McTestface", dt_1, "", "");
        add_trace(&connection, &expected_trace).unwrap();

        let before_configured_timeframe =
            NaiveDateTime::from_timestamp(Local::now().timestamp(), 0)
                - Duration::days(config.timeframe().num_days() + 1);
        let long_ago_trace = NewTrace::new("Tester Two", before_configured_timeframe, "", "");
        add_trace(&connection, &long_ago_trace).unwrap();

        let t: Vec<Trace> = recent_traces(&connection, &config).unwrap();
        assert_eq!(t.len(), 1, "Should find exactly one saved trace");
        assert_eq!(t[0].name(), "Tester McTestface");
        assert_eq!(t[0].datetime(), &dt_1);
        assert_eq!(t[0].location(), "");
        assert_eq!(t[0].comment(), "");

        // cleanup
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });
    }

    #[test]
    fn should_order_relevant_traces_newest_first() {
        let config = Config::default();
        let connection = prepare_database_connection(
            "/tmp/contact-trace-test/recent_traces_should_order_newest_first.db",
        )
        .unwrap();

        // cleanup before running in case any cleanup was not done before due to a failure
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });

        let yesterday =
            NaiveDateTime::from_timestamp(Local::now().timestamp(), 0) - Duration::days(1);
        let new_trace = NewTrace::new("Tester Two", yesterday, "", "");
        add_trace(&connection, &new_trace).unwrap();

        let now = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let old_trace = NewTrace::new("Tester McTestface", now, "", "");
        add_trace(&connection, &old_trace).unwrap();

        let t: Vec<Trace> = recent_traces(&connection, &config).unwrap();
        assert_eq!(t.len(), 2, "Should find exactly two saved traces");
        assert_eq!(t[0].datetime(), &now);
        assert_eq!(t[1].datetime(), &yesterday);

        // cleanup
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });
    }
}

#[cfg(test)]
mod test_purge_traces {
    use super::*;

    #[test]
    fn should_delete_only_traces_outside_of_timeframe() {
        let config = Config::default();
        let connection = prepare_database_connection(
            "/tmp/contact-trace-test/purge_should_delete_outside_of_timeframe.db",
        )
        .unwrap();

        // cleanup before running in case any cleanup was not done before due to a failure
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });

        let within_timeframe = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let recent_trace = NewTrace::new("Recent McTestface", within_timeframe, "", "");
        add_trace(&connection, &recent_trace).unwrap();
        let before_configured_timeframe =
            NaiveDateTime::from_timestamp(Local::now().timestamp(), 0)
                - Duration::days(config.timeframe().num_days() + 3);
        let trace_outside_of_timeframe_1 =
            NewTrace::new("Tester McTestface", before_configured_timeframe, "", "");
        add_trace(&connection, &trace_outside_of_timeframe_1).unwrap();
        let before_configured_timeframe =
            NaiveDateTime::from_timestamp(Local::now().timestamp(), 0)
                - Duration::days(config.timeframe().num_days() + 1);
        let trace_outside_of_timeframe_2 =
            NewTrace::new("Another McTestface", before_configured_timeframe, "", "");
        add_trace(&connection, &trace_outside_of_timeframe_2).unwrap();

        let num_deleted = purge_traces(&connection, &config).unwrap();
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        assert_eq!(num_deleted, 2, "Should return number of deleted traces");
        assert_eq!(t.len(), 1, "Should find one trace left in database");
        assert_eq!(t[0].name(), "Recent McTestface");

        // cleanup
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });
    }
}

#[cfg(test)]
mod test_trace_by_name_and_date {
    use super::*;

    #[test]
    fn should_find_correct_trace_even_with_similar_traces() {
        let connection = prepare_database_connection(
            "/tmp/contact-trace-test/trace_by_name_and_date_should_find_correct_trace.db",
        )
        .unwrap();

        // cleanup before running in case any cleanup was not done before due to a failure
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });

        // should not find this, even with similar date
        let dt = NaiveDateTime::from_timestamp(Local::now().timestamp(), 0);
        let new_trace = NewTrace::new("Tester McTestface", dt, "", "");
        add_trace(&connection, &new_trace).unwrap();

        let expected_trace = NewTrace::new("Wanted McTestface", dt, "test", "hello");
        add_trace(&connection, &expected_trace).unwrap();

        let result = trace_by_name_and_date(
            &connection,
            "Wanted McTestface",
            &dt.format(DATE_ARG_FORMAT).to_string(),
        )
        .unwrap()
        .unwrap();
        assert_eq!(result.name(), "Wanted McTestface");
        assert_eq!(result.datetime(), &dt);
        assert_eq!(result.location(), "test");
        assert_eq!(result.comment(), "hello");

        // cleanup
        let t: Vec<Trace> = traces.load::<Trace>(&connection).unwrap();
        t.iter().for_each(|trace| {
            diesel::delete(traces.filter(id.eq(trace.id())))
                .execute(&connection)
                .unwrap();
        });
    }
}
