table! {
    traces (id) {
        id -> Integer,
        name -> Text,
        datetime -> Timestamp,
        location -> Text,
        comment -> Text,
    }
}
