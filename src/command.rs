use std::error::Error;
use std::fs;
use std::path::PathBuf;

use chrono::NaiveDate;
use clap::ArgMatches;
use serde_json;
use serde_yaml;

use crate::constants::DATE_ARG_FORMAT;
use crate::database;
use crate::traces::group_traces_by_date;
use crate::traces::NewTrace;
use crate::Config;

// TODO: update?
pub enum Command {
    Add(NewTrace),
    Delete {
        name: String,
        date: String,
    },
    Purge,
    Report {
        filter: Option<String>,
        format: OutputFormat,
    },
    Backup {
        target: PathBuf,
        print_sql: bool,
    },
}
impl Command {
    /// Construct a `Command` from the arguments passed to the program.
    ///
    /// Not specifying any subcommand prints a report of the most recent traces within the
    /// configured timeframe.
    pub fn from_args(args: &ArgMatches) -> Result<Command, Box<dyn Error>> {
        Ok(match args.subcommand() {
            ("add", Some(sub_args)) => {
                let default_trace = NewTrace::default();
                let name = sub_args.value_of("name").unwrap_or(default_trace.name());
                let datetime = match sub_args.value_of("date") {
                    Some(date) => {
                        NaiveDate::parse_from_str(date, DATE_ARG_FORMAT)?.and_hms(0, 0, 0)
                    }
                    None => default_trace.datetime(),
                };
                let location = sub_args
                    .value_of("location")
                    .unwrap_or(default_trace.location());
                let comment = sub_args
                    .value_of("comment")
                    .unwrap_or(default_trace.comment());

                Command::Add(NewTrace::new(name, datetime, location, comment))
            }
            ("delete", Some(sub_args)) => {
                let default_trace = NewTrace::default();
                let name = String::from(
                    sub_args
                        .value_of("name")
                        .expect("Required argument missing! Check --help"),
                ); // expected to be handled by clap
                let date = match sub_args.value_of("date") {
                    Some(date) => NaiveDate::parse_from_str(date, DATE_ARG_FORMAT)?,
                    None => default_trace.datetime().date(),
                }
                .to_string();
                Command::Delete { name, date }
            }
            ("purge", _) => Command::Purge,
            ("report", Some(sub_args)) => {
                let filter: Option<String> = sub_args.value_of("filter").map(String::from);
                let format: OutputFormat =
                    OutputFormat::from(sub_args.value_of("output-format").unwrap_or("plain"));
                Command::Report { filter, format }
            }
            ("backup", Some(sub_args)) => {
                let target: PathBuf = PathBuf::from(
                    sub_args
                        .value_of("backup-target")
                        .unwrap_or(Config::default().database_backup_path_str()),
                );
                let print_sql = sub_args.is_present("print-sql");
                Command::Backup { target, print_sql }
            }
            _ => Command::Report {
                filter: None,
                format: OutputFormat::Plain,
            },
        })
    }

    /// Execute this `Command` based on the given configuration.
    pub fn execute(&self, config: &Config) -> Result<(), Box<dyn Error>> {
        let database_connection = match config.database_connection() {
            Some(con) => con,
            None => unimplemented!(),
        };
        match self {
            Command::Add(new_trace) => {
                database::add_trace(database_connection, new_trace)?;
                Ok(())
            }
            Command::Report { filter, format } => {
                let mut traces = database::recent_traces(database_connection, config)?;
                let traces_count = traces.len();
                if let Some(filter) = filter {
                    traces = traces
                        .into_iter()
                        .filter(|trace| trace.filter(filter))
                        .collect();
                }
                let grouped_by_date = group_traces_by_date(traces);
                match format {
                    OutputFormat::Plain => {
                        let mut dates: Vec<&String> = grouped_by_date.keys().collect();
                        dates.sort();
                        dates.reverse();
                        for date in dates {
                            let traces = grouped_by_date
                                .get(date)
                                .map_or_else(|| Vec::new(), |traces| traces.to_owned().to_vec());
                            println!("{} ({})", date, traces.len());
                            for trace in traces {
                                println!("  {}", trace);
                            }
                        }
                        println!("\nContacts: {}", traces_count);
                    }
                    OutputFormat::Yaml => println!("{}", serde_yaml::to_string(&grouped_by_date)?),
                    OutputFormat::Json => println!("{}", serde_json::to_string(&grouped_by_date)?),
                }
                Ok(())
            }
            Command::Purge => {
                let num_deleted = database::purge_traces(database_connection, config)?;
                let traces_pluralized = match num_deleted {
                    1 => "trace",
                    _ => "traces",
                };
                println!("Deleted {} {}", num_deleted, traces_pluralized);
                Ok(())
            }
            Command::Delete { name, date } => {
                let trace = match database::trace_by_name_and_date(database_connection, name, date)?
                {
                    Some(t) => t,
                    None => {
                        println!("Nothing found for {} on {}", name, date);
                        return Ok(());
                    }
                };
                database::delete_trace(database_connection, trace)
            }
            Command::Backup { target, print_sql } => {
                if *print_sql {
                    unimplemented!()
                } else {
                    print!(
                        "Backing up database from {:?} to {:?} ... ",
                        config.database_url(),
                        target
                    );
                    fs::copy(config.database_url(), target)?;
                    println!("done");
                }
                Ok(())
            }
        }
    }
}

pub enum OutputFormat {
    Plain,
    Yaml,
    Json,
}
impl From<&str> for OutputFormat {
    fn from(s: &str) -> OutputFormat {
        match s.to_lowercase().as_str() {
            "yaml" => OutputFormat::Yaml,
            "json" => OutputFormat::Json,
            _ => OutputFormat::Plain,
        }
    }
}
