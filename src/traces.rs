use std::collections::HashMap;
use std::default::Default;
use std::fmt;

use chrono::{Local, NaiveDateTime};
use serde::{Serialize, Serializer};

use crate::constants::DATE_ARG_FORMAT;
use crate::schema::traces;

pub fn group_traces_by_date(t: Vec<Trace>) -> HashMap<String, Vec<Trace>> {
    let mut grouped: HashMap<String, Vec<Trace>> = HashMap::new();
    for trace in t {
        let formatted_date = trace.datetime().format(DATE_ARG_FORMAT).to_string();
        grouped
            .entry(formatted_date)
            .or_insert(Vec::new())
            .push(trace);
    }
    grouped
}

fn serialize_datetime<S>(date: &NaiveDateTime, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    serializer.serialize_str(&date.format(DATE_ARG_FORMAT).to_string())
}

#[derive(Queryable, Serialize, Clone)]
pub struct Trace {
    id: i32,
    name: String,
    #[serde(serialize_with = "serialize_datetime")]
    datetime: NaiveDateTime,
    location: String,
    comment: String,
}
impl Trace {
    pub fn id(&self) -> i32 {
        self.id
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn datetime(&self) -> &NaiveDateTime {
        &self.datetime
    }
    pub fn location(&self) -> &str {
        &self.location
    }
    pub fn comment(&self) -> &str {
        &self.comment
    }

    /// Check if any of this Trace's name, location, or comment contains the given filter string
    pub fn filter(&self, filter_str: &str) -> bool {
        vec![&self.name, &self.location, &self.comment]
            .iter()
            .any(|v| v.contains(filter_str))
    }

    #[cfg(test)]
    fn new(id: i32, name: &str, datetime: NaiveDateTime, location: &str, comment: &str) -> Trace {
        let name = String::from(name);
        let location = String::from(location);
        let comment = String::from(comment);
        Trace {
            id,
            name,
            datetime,
            location,
            comment,
        }
    }
}

impl fmt::Display for Trace {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        let mut s = String::new();
        if formatter.alternate() {
            s.push_str(&format!("{}: ", self.id()));
        }
        s.push_str(self.name());
        if !self.location.is_empty() {
            s.push_str(&format!(", {}", self.location()));
        }
        if !self.comment.is_empty() {
            s.push_str(&format!(", {}", self.comment()));
        }

        write!(formatter, "{}", s)
    }
}

#[derive(Insertable)]
#[table_name = "traces"]
pub struct NewTrace {
    name: String,
    datetime: NaiveDateTime,
    location: String,
    comment: String,
}
impl NewTrace {
    pub fn new(name: &str, datetime: NaiveDateTime, location: &str, comment: &str) -> NewTrace {
        let name = String::from(name);
        let location = String::from(location);
        let comment = String::from(comment);
        NewTrace {
            name,
            datetime,
            location,
            comment,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn datetime(&self) -> NaiveDateTime {
        self.datetime.to_owned()
    }

    pub fn location(&self) -> &str {
        &self.location
    }

    pub fn comment(&self) -> &str {
        &self.comment
    }
}

impl Default for NewTrace {
    fn default() -> NewTrace {
        NewTrace {
            name: String::new(),
            location: String::new(),
            comment: String::new(),
            datetime: NaiveDateTime::from_timestamp(Local::now().timestamp(), 0),
        }
    }
}

#[cfg(test)]
mod test_trace_display {
    use super::*;

    use chrono::NaiveDate;

    #[test]
    fn should_format_trace_without_id_and_date() {
        let t = Trace::new(
            42,
            "Foo",
            NaiveDate::from_ymd(2020, 8, 22).and_hms(0, 0, 0),
            "loc",
            "comment",
        );
        let expected = "Foo, loc, comment";
        let result = format!("{}", t);
        assert_eq!(
            result, expected,
            "Should exclude id and date for default formatter in format!"
        );
        let result = t.to_string();
        assert_eq!(
            result, expected,
            "Should exclude id and date when calling to_string()"
        );
    }

    #[test]
    fn should_add_id_and_date_for_alternate_formatter() {
        let t = Trace::new(
            42,
            "Foo",
            NaiveDate::from_ymd(2020, 8, 22).and_hms(0, 0, 0),
            "loc",
            "comment",
        );
        let result = format!("{:#}", t);
        let expected = "42: Foo, loc, comment";
        assert_eq!(
            result, expected,
            "Should include id for alternate formatter"
        );
    }
}

#[cfg(test)]
mod test_group_traces_by_date {
    use super::*;

    use chrono::NaiveDate;

    #[test]
    fn should_group_traces_by_date() {
        let t: Vec<Trace> = vec![
            Trace::new(
                1,
                "foo",
                NaiveDate::from_ymd(2020, 8, 22).and_hms(0, 0, 0),
                "",
                "",
            ),
            Trace::new(
                2,
                "bar",
                NaiveDate::from_ymd(2020, 8, 22).and_hms(0, 0, 0),
                "",
                "",
            ),
            Trace::new(
                3,
                "foobar",
                NaiveDate::from_ymd(2020, 8, 21).and_hms(0, 0, 0),
                "",
                "",
            ),
        ];
        let result = group_traces_by_date(t);
        assert!(result.contains_key("2020-08-22"));
        assert_eq!(result.get("2020-08-22").unwrap().len(), 2);
        assert_eq!(result.get("2020-08-22").unwrap()[0].id(), 1);
        assert_eq!(result.get("2020-08-22").unwrap()[1].id(), 2);
        assert!(result.contains_key("2020-08-21"));
        assert_eq!(result.get("2020-08-21").unwrap().len(), 1);
        assert_eq!(result.get("2020-08-21").unwrap()[0].id(), 3);
    }

    #[test]
    fn should_return_empty_map_for_empty_traces_vec() {
        let t: Vec<Trace> = Vec::new();
        let result = group_traces_by_date(t);
        assert!(result.is_empty());
    }
}
