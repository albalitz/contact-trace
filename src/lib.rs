mod command;
mod constants;
mod database;
mod schema;
mod traces;

#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;

use std::default;
use std::error::Error;
use std::fs;
use std::path::PathBuf;

use chrono::{Duration, Local};
use clap::{crate_name, ArgMatches};
use dirs;
use serde::{Deserialize, Deserializer};
use serde_yaml;

use diesel::prelude::*;

pub use command::Command;

#[derive(Deserialize)]
pub struct Config {
    #[serde(skip)]
    config_file: PathBuf,
    #[serde(default = "default_database_url")]
    database_url: PathBuf,
    #[serde(skip)]
    database_backup_path: PathBuf,
    #[serde(skip)]
    database_connection: Option<SqliteConnection>,
    #[serde(
        deserialize_with = "deserialize_duration",
        default = "default_timeframe"
    )]
    /// The days for which a trace is considered relevant and included in reports
    timeframe: Duration,
}
impl Config {
    /// Construct a `Config` from clap's CLI arg matches.
    /// This also uses `Config::from_file` to read general configuration from a file.
    pub fn from_args(args: &ArgMatches) -> Result<Config, Box<dyn Error>> {
        let config_file: PathBuf = match args.value_of("config-file") {
            Some(path) => PathBuf::from(path),
            None => Config::default().config_file(),
        };
        let config = Config::from_file(config_file)?;

        let database_url: String = match args.value_of("database-url") {
            Some(url) => String::from(url),
            None => match config.database_url().to_str() {
                Some(url) => String::from(url),
                None => unimplemented!(),
            },
        };
        let database_connection = database::prepare_database_connection(&database_url)?;
        let timeframe: Duration = match args.value_of("timeframe") {
            Some(days) => Duration::days(days.parse::<i64>()?),
            None => config.timeframe(),
        };

        Ok(Config {
            database_connection: Some(database_connection),
            timeframe,
            ..config
        })
    }

    /// Construct a `Config` from general configuration read from a config file,
    /// or fall back to defaults, if that file doesn't exist
    fn from_file(filepath: PathBuf) -> Result<Config, Box<dyn Error>> {
        let defaults = Config::default();
        let content = match fs::read_to_string(filepath) {
            Ok(c) => c,
            Err(_) => return Ok(defaults),
        };
        let config = serde_yaml::from_str(&content)?;
        Ok(config)
    }

    pub fn config_file(&self) -> PathBuf {
        self.config_file.to_owned()
    }

    pub fn config_file_str(&self) -> &str {
        match self.config_file.to_str() {
            Some(s) => s,
            None => "config.yml",
        }
    }

    pub fn database_backup_path_str(&self) -> &str {
        match self.database_backup_path.to_str() {
            Some(path) => path,
            None => "backup.db",
        }
    }

    pub fn database_url(&self) -> PathBuf {
        self.database_url.to_owned()
    }

    pub fn database_connection(&self) -> &Option<SqliteConnection> {
        &self.database_connection
    }

    pub fn timeframe(&self) -> Duration {
        self.timeframe.to_owned()
    }
}
impl default::Default for Config {
    fn default() -> Config {
        let config_dir: PathBuf = match dirs::config_dir() {
            Some(mut cd) => {
                cd.push(crate_name!());
                cd
            }
            None => PathBuf::from("."),
        };
        let database_url: PathBuf = {
            let mut dir = PathBuf::from(&config_dir);
            dir.push(format!("{}.db", crate_name!()));
            dir
        };
        let database_backup_path: PathBuf = {
            let mut dir = PathBuf::from(&config_dir);
            dir.push(format!(
                "{}-backup-{}.db",
                crate_name!(),
                Local::now().format(constants::DATE_ARG_FORMAT)
            ));
            dir
        };
        let config_file: PathBuf = {
            let mut dir = PathBuf::from(&config_dir);
            dir.push("config.yml");
            dir
        };
        Config {
            config_file,
            database_url,
            database_backup_path,
            database_connection: None,
            timeframe: Duration::days(14),
        }
    }
}

/// used by serde when deserializing the config file
fn deserialize_duration<'de, D>(deserializer: D) -> Result<Duration, D::Error>
where
    D: Deserializer<'de>,
{
    let days = i64::deserialize(deserializer)?;
    Ok(Duration::days(days))
}

/// used by serde when deserializing the config file
fn default_timeframe() -> Duration {
    Config::default().timeframe()
}

/// used by serde when deserializing the config file
fn default_database_url() -> PathBuf {
    Config::default().database_url()
}
