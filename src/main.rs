use std::process::exit;

use clap::{crate_description, crate_name, crate_version};
use clap::{App, Arg, SubCommand};

use contact_trace;

fn main() {
    let default_config = contact_trace::Config::default();

    let args = App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(
            Arg::with_name("config-file")
                .long("--config")
                .short("-c")
                .takes_value(true)
                .value_name("FILE")
                .help("The path to your config file.")
                .default_value(default_config.config_file_str())
                .required(false)
        )
        .arg(
            Arg::with_name("database-url")
                .help("The path to your sqlite database. Will be created if it doesn't exist.")
                .long("--database-url")
                .takes_value(true)
                .value_name("DATABASE URL")
                .required(false),
        )
        .arg(
            Arg::with_name("timeframe")
                .help("Override the timeframe in days used to filter relevant traces")
                .long("--timeframe")
                .short("-t")
                .takes_value(true)
                .value_name("DAYS")
                .required(false)
        )
        .subcommand(
            SubCommand::with_name("add")
                .about("Add a new trace")
                .arg(Arg::with_name("name").help("The name of the traced person/thing/location").required(true))
                .arg(
                    Arg::with_name("date")
                        .help("An optional date")
                        .long_help("An optional date. Defaults to today if omitted. Format: YYYY-MM-DD")
                        .required(false),
                )
                .arg(
                    Arg::with_name("location")
                        .long("--location")
                        .short("-l")
                        .takes_value(true)
                        .value_name("LOCATION")
                        .help("An optional location")
                        .long_help("An optional location. Used to group traces on the same day with the same location.")
                        .required(false),
                )
                .arg(
                    Arg::with_name("comment")
                        .long("--comment")
                        .short("-c")
                        .takes_value(true)
                        .value_name("COMMENT")
                        .help("An optional comment to be attached to the trace")
                        .required(false),
                ),
        )
        .subcommand(
            SubCommand::with_name("report")
                .about("Print a report of traces within your configured timeframe")
                .arg(
                    Arg::with_name("output-format")
                        .long("--output")
                        .short("-o")
                        .takes_value(true)
                        .help("Print the report in the given format")
                        .possible_values(&["json", "yaml"])
                        .required(false)
                )
                .arg(
                    Arg::with_name("filter")
                        .long("--filter")
                        .takes_value(true)
                        .help("Only report traces matching the filter")
                        .long_help("Only report traces matching the filter. This checks if the given filter is a substring of the title, the location, or the comment.")
                        .required(false)
                )
        )
        .subcommand(
            SubCommand::with_name("purge")
                .about("Delete traces older than your configured timeframe from the database")
        )
        .subcommand(
            SubCommand::with_name("delete").about("Delete a specific trace")
                .arg(
                    Arg::with_name("name")
                        .help("The name of the trace to be deleted")
                        .required(true)
                )
                .arg(
                    Arg::with_name("date")
                        .help("The date of the trace to be deleted")
                        .long_help("The date of the trace to be deleted. Defaults to today if omitted. Format: YYYY-MM-DD")
                        .required(false),
                )
        )
        .subcommand(
            SubCommand::with_name("backup").about("Backup your database")
                .arg(
                    Arg::with_name("backup-target")
                        .help("The path where the backup database shall be saved")
                        .long_help("The path where the backup database shall be saved.\nCan't be used together with --print-sql.")
                        .required(false)
                        .conflicts_with("print-sql")
                )
                .arg(
                    Arg::with_name("print-sql")
                        .long("--print-sql")
                        .short("-p")
                        .help("Print SQL statements to restore the database manually instead of copying the SQLite file\nNOTE: not yet implemented!")
                        .long_help("Print SQL statements to restore the database manually instead of copying the SQLite file.\nCan't be used together with backup-target.\nNOTE: not yet implemented!")
                        .required(false)
                        .takes_value(false)
                        .conflicts_with("backup-target")
                )
        )
        .get_matches();
    let config = match contact_trace::Config::from_args(&args) {
        Ok(c) => c,
        Err(e) => {
            eprintln!("Error reading configuration: {}", e.to_string());
            exit(1);
        }
    };
    let command = match contact_trace::Command::from_args(&args) {
        Ok(command) => command,
        Err(e) => {
            eprintln!("Error parsing CLI args: {}", e.to_string());
            exit(1);
        }
    };
    match command.execute(&config) {
        Ok(_) => exit(0),
        Err(e) => {
            eprintln!("Error executing command: {}", e.to_string());
            exit(1);
        }
    }
}
