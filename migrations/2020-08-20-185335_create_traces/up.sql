-- Your SQL goes here

CREATE TABLE traces (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    datetime INTEGER NOT NULL,
    location TEXT,
    comment TEXT
);
