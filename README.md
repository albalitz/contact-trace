# Contact Trace
A personal contact tracing CLI for epidemics/pandemics to help you keep track of where you were or with whom you had contact in the last weeks.

Note: This is an exercise project to get a glimpse of interacting with databases in Rust while having some kind of real world usecase.
This tool does not prevent you from dying.
